## CONTENTS OF THIS FILE

-   Introduction
-   Requirements
-   Installation

## INTRODUCTION

This module uses [vis.js](http://visjs.org) to provide a graph visualization of the relations between the Taxonomy Terms and Nodes.

## REQUIREMENTS

-   [Libraries API](https://drupal.org/project/libraries)
-   [Views](https://drupal.org/project/views)
-   [Node Order](https://drupal.org/project/nodeorder)
-   [vis.js](http://visjs.org) library

## INSTALLATION

-   Download the [vis.js](https://github.com/almende/vis/archive/master.zip) library and place it under libraries directory, so the assets are available at:
-   `/libraries/vis/dist/vis.min.js`
-   `/libraries/vis/dist/vis.min.css`
-   Install the module [as usual](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).
